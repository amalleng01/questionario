$(document).ready(function(){

  console.log("Welcome to jQuery.");

  $("#new-article").click(function(){

    var titulo = $("#form-title").val();
    var description = $("#form-description").val();

    var plantilla = "";
    plantilla += "<article>";
    plantilla += "<h2>";
    plantilla += titulo;
    plantilla += "</h2>";
    plantilla += "<p>";
    plantilla += description;
    plantilla += "</p>";


    plantilla += "</article>";

    $("#content").append(plantilla);


    var img = $("#form-image").val();

    var imagen = "";
    imagen += "<img src='";
    imagen += img;
    imagen += "'></img>";


    $("#right").append(imagen);

    // Resetear los valores del formulario
    $("#form-title").val("");
    $("#form-description").val("");
    $("#form-image").val("");


    // Para evitar que la página se recargue;
    return false;

  });

});
